#include <stdio.h>

int main() {
    int input = 0, counter = 0, eingabe, stop = 0;

    printf("Bitte geben Sie die Zahlen fuer das arithmetische Mittel ein (0 zum beenden): \n");

    while (stop == 0) {
        scanf("%d", &eingabe);
        if(eingabe == 0) break;
        input += eingabe;
        counter++;
    };

    if(counter == 0) {
        printf("Das arithmetische Mittel kann aus einer Zahl nicht berechnet werden!\n");
    } else {
        printf("Das arithmetische Mittel betraegt: %i \n", input / counter);
    }

    return 0;
}